<?php

namespace Tests\Feature;

use App\Modules\Invoices\Infrastructure\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    public function testView(): void
    {
        $invoiceId = Invoice::limit(1)->get()->first()->id;

        $response = $this->get("/api/invoice/$invoiceId");

        $response->assertStatus(200);
    }

    public function testApprove(): void
    {
        $invoice = Invoice::where('status', 'draft')->limit(1)->get()->first();
        self::assertEquals('draft', $invoice->status);

        $response = $this->get("/api/invoice/approve/$invoice->id");

        $response->assertStatus(200);
        $invoice = Invoice::find($invoice->id); // fresh instance
        self::assertEquals('approved', $invoice->status);
    }

    public function testReject(): void
    {
        $invoice = Invoice::where('status', 'draft')->limit(1)->get()->first();
        self::assertEquals('draft', $invoice->status);

        $response = $this->get("/api/invoice/reject/$invoice->id");

        $response->assertStatus(200);
        $invoice = Invoice::find($invoice->id); // fresh instance
        self::assertEquals('rejected', $invoice->status);
    }

    public function testCannotChangeStatusTwice(): void
    {
        $invoice = Invoice::where('status', 'draft')->limit(1)->get()->first();
        self::assertEquals('draft', $invoice->status);

        $response = $this->get("/api/invoice/reject/$invoice->id");

        $response->assertStatus(200);
        $invoice = Invoice::find($invoice->id); // fresh instance
        self::assertEquals('rejected', $invoice->status);

        $response = $this->get("/api/invoice/reject/$invoice->id");

        $response->assertStatus(400);
    }
}
