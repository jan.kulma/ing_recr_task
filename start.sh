#! /bin/bash

set -euo pipefail

cp .env.example .env
cp .env.testing.example .env.testing
docker compose build app
docker compose up -d
docker compose run app composer install
# At this point the script will break if the DB container is not ready yet
docker compose run app php artisan migrate
docker compose run app php artisan db:seed --class='App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder'
docker compose run app php artisan migrate --env=testing
docker compose run app php artisan db:seed --env=testing --class='App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder'

echo -e "\n\n\n\n\n\n=================\n\nSetup complete\n\n=================\n\n\n"

echo -e "\nTests:"
docker compose run app php artisan test --env=testing

echo -e "\n\n\nFollowing is the list of some available invoice IDs with status === draft:"
docker compose run app php artisan tinker --execute='echo \App\Modules\Invoices\Infrastructure\Models\Invoice::limit(10)->where("status", "draft")->pluck("id")'

ID_1=$(docker compose run app php artisan tinker --execute='echo \App\Modules\Invoices\Infrastructure\Models\Invoice::limit(10)->where("status", "draft")->pluck("id")[0]')
ID_2=$(docker compose run app php artisan tinker --execute='echo \App\Modules\Invoices\Infrastructure\Models\Invoice::limit(10)->where("status", "draft")->pluck("id")[1]')

echo -e "\nEndpoints with the first ID from the above list:"
echo -e "View:    http://localhost/api/invoice/$ID_1"
echo -e "Approve: http://localhost/api/invoice/approve/$ID_1"
echo -e "Reject:  http://localhost/api/invoice/reject/$ID_1"

echo -e "\nEndpoints with the second ID from the above list:"
echo -e "View:    http://localhost/api/invoice/$ID_2"
echo -e "Approve: http://localhost/api/invoice/approve/$ID_2"
echo -e "Reject:  http://localhost/api/invoice/reject/$ID_2"


