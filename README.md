### Recruitment Task Solution
#### Jan Kulma [jan.kulma@gmail.com](mailto:jan.kulma@gmail.com)

#### Requirements and instructions
You need `docker` and `docker compose` installed. The code was tested on Ubuntu 22, in case of any issues with the setup please contact me.

1. Clone the repository.
2. In the root dir of the repository run `./start.sh`.

This will set up the project, run migrations and db seed for dev and test env, will run tests, and output some info that should be helpful with manual testing.

**The `start.sh` script can break when it'll try to execute migrations before the DB container will be ready (`Illuminate\Database\QueryException SQLSTATE[HY000] [2002] Connection refused`). If that happens please just rerun the script.**

Thanks for taking your time to review my solution.

__________________

Some remarks:

I attempted to use DDD, however I was only familiar with the term, and did not have experience using it, so most likely I failed miserably on that front. :)

Here is my reasoning for the files structure I created.
* I tried to follow the structure that was already in the repository because I think consistency and team rules are more important than external rules.
* I understood the `Api` directory as the `Interface` layer, so code that communicates, and receives communication from outside the module, so I've put there
  * `Controller`.
  * `Dto` - following example from the `Approval` module.
  * `Listener` - receives communication from outside.
  * `ViewModel` - presentation layer for outside.
  * `InvoiceApprovalFacadeInterface` - following `Approval` module.
* `Application` in the `Approval` module seems to contain business logic, so I've placed there:
  * the `InvoiceApprovalFacadeInterface` implementation, containing the business logic (or it would contain business logic, ATM it is just calling external module).
  * the `Invoice` entity which is business model representation, although it does not contain any business logic at this point.
  * `InvoiceRepositoryInterface`, but not the implementation, because of Dependency Inversion principle: higher level module/layer should depend on abstractions.
* In the `Infrastructure` I've placed all the "Laravel" code (integrates the module with the framework), the mapper (to translate the infrastructure layer to outside), and the repository (encapsulates interactions with database). 

Additionaly:
* I update the `status` in the listener, which I don't really like, but I saw the events in the `Approval` module and I guessed it was expected that I should use them.
* I don't like that my repository uses the mapper inside, I feel they should be decoupled.
* I didn't do proper DB seed and refresh between tests, I relay on the initial seed for the testing env. So when the tests would be executed > x times, there would not be enough rows in the DB that would still match criteria for some tests.  
