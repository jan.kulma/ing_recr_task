<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ViewModel;

class InvoiceProductLineViewModel
{
    public function __construct(
        public readonly string $name,
        public readonly int $quantity,
        public readonly int $unitPrice,
    ) {
    }

    public function serialize(): array
    {
        return [
            'name' => $this->name,
            'quantity' => $this->quantity,
            'unitPrice' => $this->unitPrice,
            // This calculation does not belong here, view layer should not have this kind of logic.
            // The total should be inserted to the view model already calculated.
            'total' => $this->quantity * $this->unitPrice,
        ];
    }
}
