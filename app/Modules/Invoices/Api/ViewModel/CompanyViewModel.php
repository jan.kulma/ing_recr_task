<?php

namespace App\Modules\Invoices\Api\ViewModel;

class CompanyViewModel
{
    public function __construct(
        private readonly string $name,
        private readonly string $street,
        private readonly string $city,
        private readonly string $zip,
        private readonly string $phone,
        private readonly ?string $email = null,
    ) {
    }

    public function serialize(): array
    {
        return [
            'name' => $this->name,
            'street' => $this->street,
            'city' => $this->city,
            'zip' => $this->zip,
            'phone' => $this->phone,
            'email' => $this->email ?? '',
        ];
    }
}
