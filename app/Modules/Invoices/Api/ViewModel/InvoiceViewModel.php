<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\ViewModel;

class InvoiceViewModel
{
    /**
     * @param InvoiceProductLineViewModel[] $productLines
     */
    public function __construct(
        public readonly string $number,
        public readonly string $date,
        public readonly string $dueDate,
        public readonly string $status,
        public readonly CompanyViewModel $company,
        public readonly CompanyViewModel $billedCompany,
        public readonly array $productLines
    ) {
    }

    public function serialize(): array
    {
        return [
            'number' => $this->number,
            'date' => $this->date,
            'dueDate' => $this->dueDate,
            'status' => $this->status,
            'company' => $this->company->serialize(),
            'billedCompany' => $this->billedCompany->serialize(),
            'productLines' => array_map(
                static fn(InvoiceProductLineViewModel $lineViewModel) => $lineViewModel->serialize(),
                $this->productLines
            ),
        ];
    }
}
