<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Controller;

use App\Infrastructure\Controller;
use App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Api\InvoiceApprovalFacadeInterface;
use App\Modules\Invoices\Application\Entity\Invoice;
use App\Modules\Invoices\Application\Repository\InvoiceRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends Controller
{
    public function view(string $id, InvoiceRepositoryInterface $invoiceRepository): JsonResponse
    {
        $serialized = $invoiceRepository->getViewModelById(Uuid::fromString($id))->serialize();

        return response()->json($serialized);
    }

    public function approve(string $id, InvoiceApprovalFacadeInterface $approvalFacade): JsonResponse
    {
        try {
            $uuid = Uuid::fromString($id);
            $invoiceDto = new InvoiceDto($uuid, Invoice::class);
            $approvalFacade->approve($invoiceDto);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json(['message' => 'Invoice successfully approved.']);
    }

    public function reject(string $id, InvoiceApprovalFacadeInterface $approvalFacade): JsonResponse
    {
        try {
            $uuid = Uuid::fromString($id);
            $invoiceDto = new InvoiceDto($uuid, Invoice::class);
            $approvalFacade->reject($invoiceDto);
        } catch (\Exception $exception) {
            return response()->json(['error' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json(['message' => 'Invoice successfully rejected.']);
    }
}
