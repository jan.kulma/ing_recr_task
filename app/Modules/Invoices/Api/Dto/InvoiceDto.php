<?php

namespace App\Modules\Invoices\Api\Dto;

use Ramsey\Uuid\UuidInterface;

final readonly class InvoiceDto
{
    /** @param class-string $entity */
    public function __construct(
        public UuidInterface $id,
        public string $entity,
    ) {
    }
}
