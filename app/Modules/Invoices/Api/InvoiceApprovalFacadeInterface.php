<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api;

use App\Modules\Invoices\Api\Dto\InvoiceDto;

interface InvoiceApprovalFacadeInterface
{
    public function approve(InvoiceDto $invoiceDto): void;

    public function reject(InvoiceDto $invoiceDto): void;
}
