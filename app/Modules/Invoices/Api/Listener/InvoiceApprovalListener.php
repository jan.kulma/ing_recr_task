<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Listener;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Application\Repository\InvoiceRepositoryInterface;

class InvoiceApprovalListener
{
    public function __construct(
        private readonly InvoiceRepositoryInterface $invoiceRepository
    ) {
    }

    public function onApprove(EntityApproved $entityApproved): void
    {
        $this->invoiceRepository->updateStatus(
            $entityApproved->approvalDto->id,
            StatusEnum::APPROVED
        );
    }

    public function onReject(EntityRejected $entityRejected): void
    {
        $this->invoiceRepository->updateStatus(
            $entityRejected->approvalDto->id,
            StatusEnum::REJECTED
        );
    }
}
