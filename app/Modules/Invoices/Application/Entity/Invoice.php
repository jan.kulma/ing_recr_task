<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Entity;

use Ramsey\Uuid\UuidInterface;

class Invoice
{
    private UuidInterface $id;

    private string $status;

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
