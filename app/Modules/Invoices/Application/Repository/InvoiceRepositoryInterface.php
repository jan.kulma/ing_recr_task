<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Repository;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\ViewModel\InvoiceViewModel;
use App\Modules\Invoices\Application\Entity\Invoice;
use Ramsey\Uuid\UuidInterface;

interface InvoiceRepositoryInterface
{
    public function getEntityById(UuidInterface $id): Invoice;

    public function getViewModelById(UuidInterface $id): InvoiceViewModel;

    public function updateStatus(UuidInterface $id, StatusEnum $status): void;
}
