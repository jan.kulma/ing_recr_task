<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Api\Dto\InvoiceDto;
use App\Modules\Invoices\Api\InvoiceApprovalFacadeInterface;
use App\Modules\Invoices\Application\Entity\Invoice;
use App\Modules\Invoices\Application\Repository\InvoiceRepositoryInterface;

class InvoiceApprovalFacade implements InvoiceApprovalFacadeInterface
{
    public function __construct(
        private readonly InvoiceRepositoryInterface $invoiceRepository,
        private readonly ApprovalFacadeInterface $approvalFacade,
    ) {
    }

    public function approve(InvoiceDto $invoiceDto): void
    {
        $invoiceEntity = $this->invoiceRepository->getEntityById($invoiceDto->id);
        $approvalDto = new ApprovalDto(
            $invoiceEntity->getId(),
            StatusEnum::tryFrom($invoiceEntity->getStatus()),
            Invoice::class
        );
        $this->approvalFacade->approve($approvalDto);
    }

    public function reject(InvoiceDto $invoiceDto): void
    {
        $invoiceEntity = $this->invoiceRepository->getEntityById($invoiceDto->id);
        $approvalDto = new ApprovalDto(
            $invoiceEntity->getId(),
            StatusEnum::tryFrom($invoiceEntity->getStatus()),
            Invoice::class
        );
        $this->approvalFacade->reject($approvalDto);
    }
}
