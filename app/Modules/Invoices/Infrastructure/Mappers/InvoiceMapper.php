<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Mappers;

use App\Modules\Invoices\Api\ViewModel\CompanyViewModel;
use App\Modules\Invoices\Api\ViewModel\InvoiceProductLineViewModel;
use App\Modules\Invoices\Api\ViewModel\InvoiceViewModel;
use App\Modules\Invoices\Application\Entity\Invoice;
use App\Modules\Invoices\Infrastructure\Models\Invoice as InvoiceModel;
use App\Modules\Invoices\Infrastructure\Models\InvoiceProductLine;
use Ramsey\Uuid\Uuid;

class InvoiceMapper
{
    public function mapModelToEntity(InvoiceModel $model): Invoice
    {
        $entity = new Invoice();
        $entity->setId(Uuid::fromString($model->id))
            ->setStatus($model->status);

        return $entity;
    }

    public function mapModelToViewModel(InvoiceModel $model): InvoiceViewModel
    {
        return new InvoiceViewModel(
            number: $model->number,
            date: $model->date,
            dueDate: $model->due_date,
            status: $model->status,
            company: new CompanyViewModel(
                name: $model->company->name,
                street: $model->company->street,
                city: $model->company->city,
                zip: $model->company->zip,
                phone: $model->company->phone,
                email: $model->company->email,
            ),
            billedCompany: new CompanyViewModel(
                name: $model->billedCompany->name,
                street: $model->billedCompany->street,
                city: $model->billedCompany->city,
                zip: $model->billedCompany->zip,
                phone: $model->billedCompany->phone,
                email: $model->billedCompany->email,
            ),
            productLines: $model->productLines->map(static function (InvoiceProductLine $productLine) {
                return new InvoiceProductLineViewModel(
                    name: $productLine->product->name,
                    quantity: $productLine->quantity,
                    unitPrice: $productLine->product->price,
                );
            })->toArray()
        );
    }
}
