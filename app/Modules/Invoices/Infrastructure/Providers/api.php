<?php

declare(strict_types=1);

use App\Modules\Invoices\Api\Controller\InvoiceController;
use Illuminate\Support\Facades\Route;

Route::get('/invoice/{id}', [InvoiceController::class, 'view']);

// Using GET instead of POST, so it's easier to test in the browser.
// Normally these endpoints would be obviously POST.
Route::get('/invoice/approve/{id}', [InvoiceController::class, 'approve']);
Route::get('/invoice/reject/{id}', [InvoiceController::class, 'reject']);
