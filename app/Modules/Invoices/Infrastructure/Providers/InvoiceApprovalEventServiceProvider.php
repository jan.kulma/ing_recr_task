<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Approval\Api\Events\EntityApproved;
use App\Modules\Approval\Api\Events\EntityRejected;
use App\Modules\Invoices\Api\Listener\InvoiceApprovalListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class InvoiceApprovalEventServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Event::listen(
            EntityApproved::class,
            [InvoiceApprovalListener::class, 'onApprove']
        );
        Event::listen(
            EntityRejected::class,
            [InvoiceApprovalListener::class, 'onReject']
        );
    }
}
