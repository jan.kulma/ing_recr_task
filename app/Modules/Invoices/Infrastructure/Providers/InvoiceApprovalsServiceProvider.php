<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;

use App\Modules\Invoices\Api\InvoiceApprovalFacadeInterface;
use App\Modules\Invoices\Application\InvoiceApprovalFacade;
use App\Modules\Invoices\Application\Repository\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Repositories\InvoiceRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class InvoiceApprovalsServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(InvoiceApprovalFacadeInterface::class, InvoiceApprovalFacade::class);
        $this->app->scoped(InvoiceRepositoryInterface::class, InvoiceRepository::class);
    }

    /** @return array<class-string> */
    public function provides(): array
    {
        return [
            InvoiceApprovalFacadeInterface::class,
            InvoiceRepositoryInterface::class,
        ];
    }
}
