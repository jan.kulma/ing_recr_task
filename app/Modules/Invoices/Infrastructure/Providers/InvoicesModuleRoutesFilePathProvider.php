<?php

namespace App\Modules\Invoices\Infrastructure\Providers;

class InvoicesModuleRoutesFilePathProvider
{
    public static function getPath(): string
    {
        return __DIR__ . '/api.php';
    }
}
