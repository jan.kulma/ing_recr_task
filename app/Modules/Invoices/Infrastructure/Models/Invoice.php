<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Invoice extends Model
{
    use HasFactory;

    protected $keyType = 'string';

    protected $fillable = ['status'];

    protected $with = ['company', 'billedCompany', 'productLines'];

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function billedCompany(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function productLines(): HasMany
    {
        return $this->hasMany(InvoiceProductLine::class);
    }
}
