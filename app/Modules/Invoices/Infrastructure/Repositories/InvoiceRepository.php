<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Repositories;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Api\ViewModel\InvoiceViewModel;
use App\Modules\Invoices\Application\Entity\Invoice;
use App\Modules\Invoices\Application\Repository\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Mappers\InvoiceMapper;
use App\Modules\Invoices\Infrastructure\Models\Invoice as InvoiceModel;
use Ramsey\Uuid\UuidInterface;

class InvoiceRepository implements InvoiceRepositoryInterface
{
    public function __construct(
        private readonly InvoiceMapper $invoiceMapper
    ) {
    }

    public function getEntityById(UuidInterface $id): Invoice
    {
        $model = $this->findModelById($id);

        return $this->invoiceMapper->mapModelToEntity($model);
    }

    public function getViewModelById(UuidInterface $id): InvoiceViewModel
    {
        $model = $this->findModelById($id);

        return $this->invoiceMapper->mapModelToViewModel($model);
    }

    public function updateStatus(UuidInterface $id, StatusEnum $status): void
    {
        $model = $this->findModelById($id);

        $model->update(['status' => $status->value]);
    }

    public function findModelById(UuidInterface $id): InvoiceModel
    {
        $model = InvoiceModel::find($id);
        if (!$model) {
            throw new \RuntimeException(sprintf('Invoice with ID %s not found', $id));
        }

        return $model;
    }
}
